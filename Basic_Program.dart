

void main() {

  var myName = "Tshepiso";
  var favoriteApp = "Tiktok";
  var city = "Johannesburg";

printStatements(myName, favoriteApp, city);
  
}

void printStatements(var myName, var favoriteApp, var city){

  print("My name is $myName");
  print("My favorite app is $favoriteApp");
  print("My favorite city is $city");
  
}